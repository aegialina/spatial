#!/bin/bash
audio3d="~/spatial"
alias dir="cd $audio3d"


function audio3-all
{
    code `find . -type f | grep "\.cpp$"`
    code `find . -type f | grep "\.hpp$"`
}

function audio3
{	
	o=`find . -type f | grep "\.cpp$"`
	for obj in $o
		do
		if [ `dirname $obj` = "." ]
			then
			code $obj
		fi
	done
    o=`find . -type f | grep "\.hpp$"`
	for obj in $o
		do
		if [ `dirname $obj` = "." ]
			then
			code $obj
		fi
	done
}

if [ $# -eq 0 ]
    then
    audio3
elif [ $1 = "all" ]
	then
	audio3-all
elif [ $1 = "run" ]
    then
	if [ -e build/audio.soft ]
		then
		./build/audio.soft
		rm ./build/audio.soft
	else
		chmod u+x ./build/build.sh
		./build/build.sh
		./build/audio.soft
	fi
fi
