#pragma once
#include "import.hpp"

PyMODINIT_FUNC PyInit_Vec3(void);
PyMODINIT_FUNC PyInit_Mat3x3(void);
PyMODINIT_FUNC PyInit_Binaural(void);
PyMODINIT_FUNC PyInit_audio3d(void);
