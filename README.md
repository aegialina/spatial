# Spatial

Spatial プロジェクトは`spatial`モジュールの輸入だけで動作するようにするOpenALを利用した空間音響プロジェクトです。`.wav`、`.mp3`に対応するようにしています。

Windows、OSX、Linux(Chromebook-Crostini)、BSDsで動作するクロスプラットフォーム対応を目指しています。なお、多くのプラットフォームにおける単体テストはしておりませんので、動かない場合もございますことをご了承ください。

# 開発必要ライブラリ
- ffmpeg
    * avcodec
    * avformat
    * avutil 
    * swresample
- openal
- alut
