#include "audio.hpp"
#include "__app__.hpp"
constexpr auto vec3 = "Vector";
constexpr auto mat3x3 = "Matrix";
constexpr auto binaural = "Binaural";
#define stradd(a,b) std::string(a,b).c_str()   

namespace Vectordef
{
    static void None() { ; }
    inline static PyObject* New(Vector3& self) { return PyCapsule_New(&self, vec3, nullptr); }
    inline static Vector3* getPtr(PyObject* self) { return (Vector3*)PyCapsule_GetPointer(self, vec3); }
    static PyObject* __come_on(PyObject*self, PyObject*args)
    {
        float arg[Vector3::len]={0,0,0};
        Vector3 vec;
        int fi=PyArg_ParseTuple(args, "f|ff", &arg[Vector3::first],&arg[Vector3::second],&arg[Vector3::third]);
        if(fi>0){vec.set(arg[Vector3::first],arg[Vector3::second],arg[Vector3::third]);}
        return New(vec);
    }

    static PyObject* __set(PyObject*self, PyObject*args,PyObject *keywds)
    {
        static const char* kwdlsts[]={"x","y","z",nullptr};
        static char** kwdlst = kwdlst;
        auto vec=getPtr(self);
        if (!PyArg_ParseTupleAndKeywords(args, keywds, "i|fff", kwdlst, &vec->x, &vec->y, &vec->z))
        {
            return self;
        }
        return self;
    }

    static const char* new_obj()
    {
        std::string ret = "allocate a ";
        ret += vec3;
        return ret.c_str();
    }

    static PyMethodDef methods[] = 
    {
        {vec3, __come_on , METH_VARARGS,new_obj() },
        {"set",(PyCFunction)__set,METH_VARARGS, "none"},
        {nullptr, nullptr, FALSE, nullptr}
    };

    static struct PyModuleDef module = 
    {
        PyModuleDef_HEAD_INIT,
        vec3,              // name of this module
        "Vector3",  // Doc String
        -1,
        methods
    };
}

namespace Matrixdef
{
    inline static PyObject* New(Matrix3x3& self) { return PyCapsule_New(&self, mat3x3, nullptr); }
    inline static Matrix3x3* getPtr(PyObject* self) { return (Matrix3x3*)PyCapsule_GetPointer(self, mat3x3); }
    
    static PyObject* __come_on(PyObject* self, PyObject* args)
    {
        auto noneptr = (Vector3*)&Vector3::none;
        Vector3* arg[Matrix3x3::len]{noneptr,noneptr,noneptr};
        PyObject* objs[Matrix3x3::len]{ Py_None ,Py_None ,Py_None};
        Matrix3x3 mat;
        int fi = PyArg_ParseTuple(args, "O|OO", &objs[Matrix3x3::first], &objs[Matrix3x3::second], &objs[Matrix3x3::third]);
        for (int i = 0; i < Matrix3x3::len; i++)
        {
            if (objs[i]==Py_None){break;}
            arg[i] = Vectordef::getPtr(objs[i]);
        }
        if (fi > 0) { mat.set(*arg[Matrix3x3::first], *arg[Matrix3x3::second], *arg[Matrix3x3::third]); }
        return New(mat);
    }

    static PyObject* __set(PyObject* self, PyObject* args, PyObject* keywds)
    {
        auto noneptr = (Vector3*)&Vector3::none;
        Vector3* arg[Matrix3x3::len]{ noneptr,noneptr,noneptr };
        PyObject* objs[Matrix3x3::len]{ Py_None ,Py_None ,Py_None };
        static const char* kwdlsts[] = { "x","y","z",nullptr };
        static char** kwdlst = kwdlst;
        auto mat = getPtr(self);
        if (!PyArg_ParseTupleAndKeywords(args, keywds, "iO|OO", kwdlst, objs[Matrix3x3::first], objs[Matrix3x3::second], objs[Matrix3x3::third]))
        {
            return self;
        }
        for (int i = 0; i < Matrix3x3::len; i++)
        {
            if (objs[i] == Py_None) { break; }
            arg[i] = Vectordef::getPtr(objs[i]);
        }
        mat->set(*arg[Matrix3x3::first], *arg[Matrix3x3::second], *arg[Matrix3x3::third]);
        return self;
    }

    static const char* new_obj()
    {
        std::string ret = "allocate a ";
        ret += mat3x3;
        return ret.c_str();
    }

    static PyMethodDef methods[] =
    {
        {mat3x3, __come_on , METH_VARARGS, new_obj()},
        {"set",(PyCFunction)__set,METH_VARARGS | METH_KEYWORDS, "none"},
        {nullptr, nullptr, FALSE, nullptr}
    };

    static struct PyModuleDef module =
    {
        PyModuleDef_HEAD_INIT,
        mat3x3,              // name of this module
        "Matrix3x3",  // Doc String
        -1,
        methods
    };
}

namespace Binauraldef
{

    inline static binauralLib* getPtr(PyObject* self){return (binauralLib*)PyCapsule_GetPointer(self, binaural);}
    inline static PyObject* New(binauralLib& self) { return PyCapsule_New(&self, binaural, nullptr); }

    static PyObject ptr __lib(PyObject*self, PyObject*args)
    {
        binauralLib al;
        return New(al);
    }

    static PyObject ptr binaural_load(PyObject*self, PyObject*args)
    {
        PyObject ptr str;
        std::string path;
        auto bina = getPtr(self);
        int fi=PyArg_ParseTuple(args, "|O",&str);
        bool ok=false;
        PyUnicode_AsEncodedString(str,path.c_str(),"");
        ok=bina->Init(path)();
        return ok?Py_True:Py_False;
    }

    static PyObject ptr __play(PyObject*self, PyObject*args)
    {
        auto bina=getPtr(self);
        bina->Play();
        return self;
    }

    static PyObject ptr __play3d(PyObject* self, PyObject* args)
    {
        PyObject ptr obj=nullptr;
        auto bina = getPtr(self);
        int fi=PyArg_ParseTuple(args, "O",&obj);
        auto mat = Matrixdef::getPtr(obj);
        if (bina&&mat) {
            bina->Play3D(*mat);
        }
        return self;
    }
    
    static const char* new_obj()
    {
        std::string ret = "allocate a ";
        ret += binaural;
        return ret.c_str();
    }

    static PyMethodDef methods[] = 
    {
        {binaural, __lib , METH_VARARGS, new_obj()},
        {"play",__play,METH_VARARGS, "none"},
        {"play3d",__play3d,METH_VARARGS, "none"},
        {nullptr, nullptr, FALSE, nullptr}
    };

    static struct PyModuleDef module = 
    {
        PyModuleDef_HEAD_INIT,
        binaural,              // name of this module
        "Binaural",  // Doc String
        -1,
        methods
    };

}

PyMODINIT_FUNC PyInit_Vec3(void) 
{
    return PyModule_Create(&Vectordef::module);
}

PyMODINIT_FUNC PyInit_Mat3x3(void)
{
    return PyModule_Create(&Matrixdef::module);
}

PyMODINIT_FUNC PyInit_Binaural(void) 
{
    return PyModule_Create(&Binauraldef::module);
}

PyMODINIT_FUNC PyInit_audio3d(void) 
{
    return PyInit_Binaural();
}