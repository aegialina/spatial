#!/bin/bash
BUILD_CXX="clang++"
CO_OPT="-w -O2 -std=c++2a"
LIBS="-lavcodec -lavformat -lavutil -lswresample -lopenal -lalut"
OUT_NAME="audio.soft"
BUILD_SOURCES=()
if [ "$1" = "-ulib" -o "$1" = "all" ]
    then
    BUILD_SOURCES+=("../libs/Kdjson/json11.cpp ../libs/Kdjson/json11.hpp")
fi
BUILD_SOURCES+=("../__app__.cpp")
BUILD_SOURCES+=("../audio.cpp")
BUILD_O="audio.o json11.o"
cd ~/spatial/build
for BUILD_SOURCE in "${BUILD_SOURCES[@]}"
    do
    ${BUILD_CXX} ${CO_OPT} -c ${BUILD_SOURCE}
done
${BUILD_CXX} ${BUILD_O} ${LIBS} -o ${OUT_NAME}
