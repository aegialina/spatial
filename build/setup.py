from distutils.core import setup, Extension
from json import encoder
import os
import json

module=None

if os.getcwd() == "/home/kd1278181/spatial/build":
    file="cfg.json"
else:
    file="build/cfg.json"

with open(file,encoding="utf-8") as f:
    jobj=json.load(f)
    module=jobj

if module is None:
    module=["audio.cpp","libs/Kdjson/json11.cpp","__app__.cpp"]

setup(
    name='binaural',
    version='1.0',
    ext_modules=[Extension('Binaural',module)]
)