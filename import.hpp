﻿#pragma once
#define PY_SSIZE_T_CLEAN
#if defined(__linux__)|| defined(BSD)
//LINUX OR BSDS(Clang++) 
#if __has_include(<python3.9m/Python.h>)
#include <python3.9m/Python.h>
#elif __has_include(<python3.8m/Python.h>)
#include <python3.8m/Python.h>
#elif __has_include(<python3.7m/Python.h>)
#include <python3.7m/Python.h>
#elif __has_include(<python3.6m/Python.h>)
#include <python3.6m/Python.h>
#endif
#if __has_include(<audio/wave.h>)
#define HAS_WAV <audio/wave.h>
#include HAS_WAV
#endif 
#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alut.h>
#endif
#ifdef MACOSX
//APPLE OSX(Clang++)
#include <OpenAL/al.h>
#include <OpenAL/alc.h>
#include <OpenAL/alut.h>
#include <Python.h>
#endif
#ifdef _MSC_VER
//WINDOWS(MSVC)
#define _USE_MATH_DEFINES
#include <al.h>
#include <alc.h>
#include <alut.h>
#ifdef _DEBUG
#undef _DEBUG
#include <Python.h>
#define _DEBUG
#else
#include <Python.h>
#endif
#if VC_DLL_EXPORTS
#undef VC_DLL_EXPORTS
#define VC_DLL_EXPORTS extern "C" __declspec(dllexport)
#else
#define VC_DLL_EXPORTS extern "C" __declspec(dllimport)
#endif
#define Import VC_DLL_EXPORTS
#endif
#if __has_include(<unistd.h>)
//ALL POSIX(LINUX,BSDS,APPLE OSX,etc...)
#define IS_POSIX true
#elif __has_include(<windows.h>)
#define IS_WIN true
#include <Windows.h>
#endif
#ifdef IS_POSIX
//POSIX
constexpr auto ln = "\n";
constexpr auto tab = "\t";
constexpr auto alm = "\a";
#elif IS_WIN
//WINDOWS
constexpr auto ln = "\n";
constexpr auto tab = "\t";
constexpr auto alm = "\a";
#pragma warning(disable : 4996)
#endif
#if defined(__x86_64__)||defined(_WIN64)
//Windows=_WIN64 POSIX=__x86_64__
#define X64
#elif defined(__i386__)||defined(_WIN32)
//Windows=_WIN32 POSIX=__i386__
#define X32
#elif defined(__aarch64__)
//Apple Silicon or Arm Chromebook Platform
#define ARM64
#endif
#if defined(IS_WIN)
#pragma comment(lib, "libs\\Kdjson\\x64\\Release\\Kdjson.lib")
#pragma comment(lib, "OpenAL32.lib")
#pragma comment(lib, "python3.lib")
#pragma comment(lib, "python39.lib")
//#pragma comment(lib, "")
#endif // 

//COMMON
#include <any>
#include <string>
#include <cstdio>
#include <memory>
#include <functional>
#include <cmath>
#include <stdlib.h>
#include <array>
#include <vector>
#include <unordered_map>
#include <type_traits>
#include <typeindex>
#include <map>
#include <tuple>
#include <iostream>
#include <iosfwd>
#include <fstream>
#include <chrono>
#include <thread>
#include <variant>
#include <unordered_map>
#include <utility>
#include <limits.h>
#include <algorithm>
#include <cctype>
#include <sstream>
#include "libs/bin/import.hpp"

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/avutil.h>
#include <libavutil/opt.h>
#include <libswresample/swresample.h>
};

#ifdef IS_POSIX
#include <sys/stat.h>
#endif // IS_POSIX


#ifdef IS_WIN
#include <Shlwapi.h>
#pragma comment(lib, "Shlwapi.lib")
#pragma comment(lib,"../libs/ffmpeg-lgpl-v4.4/lib/avcodec.lib")
#pragma comment(lib,"../libs/ffmpeg-lgpl-v4.4/lib/avformat.lib")
#pragma comment(lib,"../libs/ffmpeg-lgpl-v4.4/lib/avutil.lib")
#pragma comment(lib,"../libs/ffmpeg-lgpl-v4.4/lib/swresample.lib")
#endif

//可読性優先のためstructではなくinterfaceを定義する
#ifndef interface
#define interface struct
#endif
#ifndef abstract
#define abstract	 
#endif // !abstract

#ifndef elif 
#define elif else if
#endif
#ifndef then
#define then ?
#endif // 
#ifndef other
#define other :
#endif // other
#ifndef ptr
#define ptr *
#endif // !ptr
#ifndef $self
#define $self (*this)
#endif // $self
#ifndef $ret
#define $ret return $self;
#endif // $ret
#ifndef thisptr_t
#define thisptr_t decltype(this)
#endif // !thisptr_t
#ifndef selfref_t
#define selfref_t decltype($self)
#endif // !selfref_t
#ifndef self_t
#define self_t std::remove_reference<decltype($self)>::type
#endif // !self_t
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif
#ifdef BOOL
#undef BOOL
#endif // BOOL
#define AS_REF [&]
#define AS_NEW [=]
#define function(A)	[](A)
#define prop_init(i,get,set) make_function<prop##i>(std::function<prop##i&()>([&]()->prop##i&{return get();}),std::function<void(const prop##i&)>([&](const prop##i& arg)->void{set(arg);return;}))

constexpr auto If = [](bool A) {return A; };

class UnityWithApi;
class LogErr;
class ScrLog;
using std::make_unique;
using std::make_shared;
using pch = const char*;
using pcch = const char* const;
using ScrOut=LogErr;
using PrintOut=ScrLog;

template<typename T>
using Let = const T&;

#ifndef std::any
#define NOT_DEF_ANY
#else
#define DEF_ANY
#endif // !std::any
void __write(void* self);

class TypeObject
{
public:
    TypeObject(const std::type_info& i)
        :info(&i)
    {
    }
    inline TypeObject& operator=(const TypeObject& o)
    {
        info = o.info;
        return *this;
    }
    inline const bool operator==(const TypeObject& o)
        const
    {
        return (*info) == (*o.info);
    }
    inline operator const std::type_info& ()
        const
    {
        return *info;
    }

private:
    const std::type_info* info;
};

template<typename T, typename... TList>
using func_t = std::function<T(TList...)>;
template<typename T, typename... TList>
using func_pt = T(*)(TList...);
using classbase = std::pair<void*, TypeObject>;

template<typename T>
class PropBase
{
public:
    PropBase(T* prop)
        :p(prop){}
    PropBase(const PropBase& prop)
        :p(prop.p){}
    ~PropBase(){}
    inline virtual T& operator*(){return *p;}
    inline virtual void operator=(const T& prop){}
    inline virtual operator T() { return *p; }
    inline virtual void _set(const T& prop){}
    const void* const pointer()
        const
    {
        return p;
    }
protected:
    void set(const T& prop)
    {
        *p = prop;
    }
    T* get()
    {
        return p;
    }
private:
    T* p;
};

template<typename T>
class PropGet :public PropBase<T>
{
public:
    PropGet(T* prop)
        :PropBase<T>(prop) {}
    PropGet(const PropGet& prop)
        :PropBase<T>(prop) {}
private:

};

template<typename T>
class PropSet:public PropBase<T>
{
public:
    PropSet(T* prop)
        :PropBase<T>(prop){}
    PropSet(const PropSet& prop)
        :PropBase<T>(prop){}
    inline virtual void _set(const T& prop) 
    {
        this->set(prop);
    }
    inline void operator=(const T& prop)
        override
    {
        this->set(prop);
    }
};

template<typename T>
class FnProp
{
public:
    using get_t = std::function<T&()>;
    using set_t = std::function<void(const T&)>;
    class c_get_t
    {
    public:
        template <class _Fx, class Self>
        c_get_t(_Fx&& func,Self thisptr)
            :binder(std::bind(func, thisptr))
        {
        }
        ~c_get_t(){}
        operator get_t& () { return binder; }
    private:
        get_t binder;
    };

    class c_set_t
    {
    public:
        template <class _Fx, class Self>
        c_set_t(_Fx&& func, Self thisptr)
            :binder(std::bind(func,thisptr,std::placeholders::_1))
        {}
        ~c_set_t() {};
        operator set_t& () { return binder; }
    private:
        set_t binder;
    };

    FnProp(get_t _get)
        :get(_get)
        ,set(nullptr)
    {
    }
    FnProp(get_t _get,set_t _set)
        :get(_get)
        ,set(_set)
    {
    }

    template <typename Self,typename class_get,typename class_set>
    FnProp(Self* thisptr, class_get&& _get,class_set&& _set)
        :get(c_get_t(_get,thisptr))
        ,set(c_set_t(_set,&thisptr))
    {
    }
    bool isSetNone()
    {
        return set == nullptr;
    }
    inline virtual T* operator*() {
        if (set) {
            return &get(); 
        }
        else { return nullptr; }
    }
    const void* const pointer()
        const
    {
        return &get();
    }
    get_t get;
    set_t set;
};

class bad_property : public std::runtime_error
{
    public:     //元々std::runtime_errorにある文字列↓   ↓追加の情報
    bad_property()
        :runtime_error("exception:bad property")//こうすることでwhat()で人間向けメッセージが読める
    {}
private:
};

template<typename T>
class Property
{
public:
    using get_t = PropGet<T>;
    using set_t = PropSet<T>;
    using prop_t = FnProp<T>;
private:
    virtual bool pisnull_get()
    {
        auto my_var=std::get_if<std::nullptr_t>(&var);
        return my_var==nullptr then false other true;
    }

    virtual get_t* pmaz_get()
    {
        return std::get_if<get_t>(&var);
    }

    virtual set_t* pmaz_set()
    {
        return std::get_if<set_t>(&var);
    }

    virtual prop_t* pmaz_prop()
    {
        return std::get_if<prop_t>(&var);
    }

    virtual const get_t* cmaz_get()
        const
    {
        return std::get_if<get_t>(&var);
    }

    virtual const set_t* cmaz_set()
        const
    {
        return std::get_if<set_t>(&var);
    }

    virtual const prop_t* cmaz_prop()
        const
    {
        return std::get_if<prop_t>(&var);
    }

    virtual T& all_house()
    {
        auto a = pmaz_get();
        auto b = pmaz_set();
        auto c = pmaz_prop();
        if (a)
        {
            return a->operator*();
        }
        elif(b)
        {
            return b->operator*();
        }
        else
        {
            return c->get();
        }

    }

    const void* const pointer()
    {
        if (pisnull_get()) { return nullptr; }
        const void* v = nullptr;
        auto a = cmaz_get();
        auto b = cmaz_set();
        auto c = cmaz_prop();
        if (a)
        {
            v=a->pointer();
        }
        elif(b)
        {
            v=b->pointer();
        }
        else
        {
            v=c->pointer();
        }
        return v;
    }

public:

    Property() 
        :var(nullptr)
        ,$adr(nullptr)
    {}

    Property(get_t p)
        :var(p)
        ,$adr(p.pointer())
    {
    }

    Property(set_t p)
        :var(p)
        , $adr(p.pointer())
    {
    }

    Property(prop_t p)
        :var(p)
         , $adr(p.pointer())
    {
    }

    /*virtual std::unordered_map<std::string,std::function>* pmaz_prop()
    {
        return std::get_if<std::unordered_map<std::string,std::function>>(&var);
    }*/

    virtual bool isNone()
    {
        return $adr == nullptr;
    }

    virtual T operator&()
    {
        return all_house();
    }

    virtual void operator=(const std::nullptr_t null)
    {
        auto a = pmaz_get();
        auto b = pmaz_set();
        auto c = pmaz_prop();
        auto var = T();
        if (a)
        {
            return a->_set(var);
        }
        elif(b)
        {
            return b->_set(var);
        }
        else
        {
            if (c->set)
            {
                return c->set(var);
            }
            return;
        }
    }

    virtual void operator=(const T& prop) 
    {
        auto a = pmaz_get();
        auto b = pmaz_set();
        auto c = pmaz_prop();
        if (a)
        {
            return a->_set(prop);
        }
        elif(b)
        {
            return b->_set(prop);
        }
        else
        {
            if (c->set) 
            {
                return c->set(prop);
            }
            return;
        }
    }

    virtual Property<T>& operator=(Property<T> prop)
    {
        this->var = prop.var;
        this->$adr = pointer();
        return *this;
    }

    virtual T* operator->()noexcept(false)
    {
        auto a = pmaz_get();
        auto b = pmaz_set();
        auto c = pmaz_prop();
        if (a)
        {
            throw bad_property();
        }
        elif(b)
        {
            return &(b->operator*());
        }
        return c->operator*();
    }
    
    virtual operator T() 
    {
        return all_house();
    }

    const void* $adr;

private:
    std::variant<get_t,set_t,prop_t,std::nullptr_t> var;
};

template<typename T>
Property<T> make_getter(T& v)
{
    return Property<T>(PropGet<T>(&v));
}

template<typename T>
Property<T> make_setter(T& v)
{
    return Property<T>(PropSet<T>(&v));
}

template<typename T>
using f_get_t = typename FnProp<T>::get_t;
template<typename T>
using f_set_t = typename FnProp<T>::set_t;

template<typename T>
Property<T> make_function(f_get_t<T> get,f_set_t<T> set=nullptr)
{
    return Property<T>(FnProp<T>(get, set));
}

template <typename T,typename Self, typename class_get=T(Self::*&&)(), typename class_set=void(Self::*&&)(T&)>
Property<T> make_function(Self* thisptr, class_get _get, class_set _set)
{
    return Property<T>(FnProp<T>(thisptr, _get, _set));
}

/*
template<typename T, typename... TList>
func_t<T, TList...> func(void* p)
{
    return (func_pt<T, TList...>)p;
}

template<typename T, typename... TList>
void* func(func_t<T, TList...>& p)
{
    auto ret = p.target<func_pt<T, TList...>>();
    return (void*)ret == nullptr ? nullptr : *ret;
}

template<typename T> class ClassType;

template<typename T>
class Function
{
    Function()
    {
        fn = []() {};
    }
    template<typename... Tys>
    Function(void(*)(ClassType<T>*,Tys*...),ClassType<T>& self,Tys*... arg)
    {
        fn=std::bind(self,arg...);
    }
private:
    std::function<void()> fn;
};

template<typename T>
class ClassType:public std::unordered_map<T, Function<T>>;
{
public:
    ClassType() {
        myclass = MyClass();
    }
    ClassType(MyClass my) {
        myclass = my;
    }
    ~ClassType() {}
    ClassType operator+(const ClassType& c)
        const
    {
        auto ret = ClassType(*this);
        auto iter = c.myclass.begin();
        while (iter != c.myclass.end()) {
            ret.myclass.insert(*iter);
            ++iter;
        }
        return ret;
    }
    ClassType& operator+=(const ClassType& c)
    {
        auto iter = c.myclass.begin();
        while (iter != c.myclass.end()) {
            this->myclass.insert(*iter);
            ++iter;
        }
        $ret;
    }
    Function& operator[](const T name)
    {
        if (this->myclass.count(name))
            return this->myclass[name];
        else
            return f;
    }
    template<typename T1, typename... TList>
    ClassType& set(const T name, func_pt<T1, TList...> v)
    {
        this->myclass[name] = Function(v);
        $ret
    }
    template<typename T1, typename... TList>
    int operator()(func_pt<T1, TList> fn)
    {
        return(int)(void*)fn;
    }
    virtual const ClassPtr& print()const { return* p; }
protected:
    MyClass myclass;
    ClassPtr* p = nullptr;
    static Function f = Function();
};

class ClassString :public ClassType<std::string>
{
private:
public:
    ClassString()
        :ClassType()
    {
        this->myclass[""] = Function();
    }
    ClassString(MyClass my)
        :ClassType(my)
    {
        this->myclass[""] = Function();
    }
    ~ClassString() {}
    Function& operator[](const std::string name)
    {
        if (this->myclass.count(name))
            return this->myclass[name];
        else
            return this->myclass[""];
    }
};

class ClassInt :public ClassType<int>
{
private:
public:
    ClassInt()
        :ClassType()
    {
        this->myclass[0] = Function();
    }
    ClassInt(MyClass my)
        :ClassType(my)
    {
        this->myclass[0] = Function();
    }
    ~ClassInt() {}
    Function& operator[](const int name)
    {
        if (this->myclass.count(name))
            return this->myclass[name];
        else
            return this->myclass[0];
    }
};

class ClassFloat :public ClassType<float>
{
private:
public:
    ClassFloat()
        :ClassType()
    {
        this->myclass[0] = Function();
    }
    ClassFloat(MyClass my)
        :ClassType(my)
    {
        this->myclass[0] = Function();
    }
    ~ClassFloat() {}
    Function& operator[](const float name)
    {
        if (this->myclass.count(name))
            return this->myclass[name];
        else
            return this->myclass[0];
    }
};

class ClassChar :public ClassType<char>
{
private:
public:
    ClassChar()
        :ClassType()
    {
        this->myclass['\0'] = Function();
    }
    ClassChar(MyClass my)
        :ClassType(my)
    {
        this->myclass['\0'] = Function();
    }
    ~ClassChar() {}
    Function& operator[](const char name)
    {
        if (this->myclass.count(name))
            return this->myclass[name];
        else
            return this->myclass['\0'];
    }
};

class ClassPtr :public ClassType<const void*>
{
private:
public:
    ClassPtr()
        :ClassType()
    {
        this->myclass[nullptr] = Function();
        this->p = this;
    }
    ClassPtr(MyClass my)
        :ClassType(my)
    {
        this->myclass[nullptr] = Function();
        this->p = this;
    }
    ~ClassPtr() {}
    Function& operator[](const void* name)
    {
        if (this->myclass.count(name))
            return this->myclass[name];
        else
            return this->myclass[nullptr];
    }
    const ClassPtr& print()
        const
    {
        for (auto &adr : this->myclass)
        {
            auto a = (size_t)adr.first;
            if (adr.first == nullptr) { continue; }
            printf("%d\n", (int)a);
        }
        $ret
    }
};
*/

;
