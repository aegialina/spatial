﻿#include "import.hpp"
#pragma once
#ifdef Import
//Import void _stdcall swr_free(SwrContext** s);
//Import int _stdcall swr_init(SwrContext* s);
//Import SwrContext* _stdcall swr_alloc();
//Import int _stdcall swr_convert(struct SwrContext* s, uint8_t** out, int out_count,const uint8_t** in, int in_count);

#endif 

class LogErr
{
	using this_t = LogErr;
	static void WriteOnce(){}
	template<typename T1, typename... T2>
	static void WriteOnce(T1 out_of, T2... obj)
	{
		std::cerr << out_of;
		this_t::WriteOnce(obj...);
	}
public:
	template<typename... T>
	static void Write(T... obj)
	{
		this_t::WriteOnce(obj...);
	}
	template<typename... T>
	static void WriteLine(T... obj)
	{
		this_t::WriteOnce(obj...);
		std::cerr << std::endl;
	}
};

class ScrLog
{
	using this_t = ScrLog;
	static void WriteOnce(){}
	static void ReadOnce(){}
	template<typename T1,typename... T2>
	static void WriteOnce(T1 out_of,T2... obj)
	{
		std::cout << out_of;
		this_t::WriteOnce(obj...);
	}
	template<typename T1, typename... T2>
	static void ReadOnce(T1& out_of, T2&... obj)
	{
		std::cin >> out_of;
		this_t::ReadOnce(obj...);
	}
public:
	template<typename... T>
	static void Write(T... obj)
	{
		this_t::WriteOnce(obj...);
	}
	template<typename... T>
	static void WriteLine(T... obj)
	{
		this_t::WriteOnce(obj...);
		std::cout << std::endl;
	}
	template<typename... T>
	static void Read(std::string message, T&... obj)
	{
		std::cout << message;
		this_t::ReadOnce(obj...);
	}
	template<typename... T>
	static void ReadLine(std::string message, T&... obj)
	{
		std::cout << message << std::endl;
		this_t::ReadOnce(obj...);
	}
private:

};

template<typename... T>
void Print(std::nullptr_t null,T... obj)
{
	PrintOut::Write(obj...);
}

template<typename... T>
void Print(T... obj)
{
	PrintOut::WriteLine(obj...);
}

template<typename... T>
void Put(std::nullptr_t null,const std::string& s, T&... obj)
{
	PrintOut::Read(s,obj...);
}

template<typename... T>
void Put(const std::string& s,T&... obj)
{
	PrintOut::ReadLine(s,obj...);
}

template<typename T>
void Putln(const std::string& s)
{
	T obj;
	PrintOut::ReadLine(s, obj);
}

template<typename T>
class Ref
{
protected:
	enum IsPtr
	{
		none,
		sp,
		wp,
	};
	std::shared_ptr<T> value;
	std::weak_ptr<T> w_value;
	IsPtr tip;
	bool __eq_value(std::shared_ptr<T>self_o,std::shared_ptr<T> elvals)
	{
		if(!self_o || !elvals) { return false; }
		elif(*self_o == *elvals) { return true; }
		else { return false; }
	}

	std::shared_ptr<T> __this_value()
		const
	{
		switch (tip)
		{
		case none:
			return nullptr;
		case sp:
			return value;
		case wp:
			return w_value.lock();
		default:
			return nullptr;
		}
	}
	bool __is_value(Ref<T> obj)
	{
	}

public:
	Ref(std::nullptr_t null)
		: value(null)
		, w_value()
		, tip(IsPtr::none)
	{
	}
	Ref()
		: value(nullptr)
		, w_value()
		, tip(IsPtr::none)
	{
	}
	Ref(const T& obj)
		: value(std::shared_ptr<T>(new T(obj)))
		, w_value()
		, tip(IsPtr::sp)
	{
	}
	Ref(const Ref<T>& obj)
		: value(obj.tip == sp ? value : obj.w_value)
		, w_value()
		, tip(IsPtr::sp)
	{
	}
	inline Ref<T> copy()
	{
		Ref<T> ret = nullptr;
		ret.w_value = value;
		ret.tip = IsPtr::wp;
		return ret;
	}
	inline Ref<T> move()
	{
		if (tip == IsPtr::sp)
		{
			w_value = value;
			value = nullptr;
			tip = IsPtr::wp;
		}
		else if (IsPtr::wp)
		{
			value = w_value.lock();
			w_value = nullptr;
			tip = IsPtr::sp;
		}$ret
	}
	inline explicit operator T& () 
	{
		auto v = __this_value();
		if (v)
			return *v;
		else
			return T();
	}
	inline explicit operator bool() 
	{
		auto v = __this_value();
		if (v)
			return bool(*v);
		else
			return false;
	}
	inline Ref<T> operator=(Ref<T> obj)
	{
		switch (tip)
		{
		case sp:
			tip=wp;
			value=nullptr;
			if(obj.tip==sp){
				w_value = obj.value;
			}
			elif(obj.tip==wp)
			{
				w_value = obj.w_value.lock();
			}
			break;
		default:
			tip=wp;
			if(obj.tip==sp){
				w_value = obj.value;
			}
			elif(obj.tip==wp)
			{
				w_value = obj.w_value.lock();
			}break;
		}
		return $self;
	}
	inline Ref<T> operator=(const T& obj)
	{
		value = std::shared_ptr<T>(new T(obj)); $ret
	}
	inline Ref<T> operator=(std::nullptr_t null)
	{
		value = nullptr;$ret
	}
	bool operator==(const Ref<T> obj)
	{

		std::shared_ptr<T> oval = __this_value();
		std::shared_ptr<T> selval = obj.__this_value();
		if (oval==selval) { return true; }
		return __eq_value(selval, oval);
	}
	void purge()
	{
		if (value) { value==nullptr; }
		if(w_value) { w_value.reset(); }
	}
	inline bool is(const Ref<T> obj)
	{
		return value == obj.value;
	}
	inline bool operator==(const T& obj)
	{
		if (value == nullptr) { return false; }
		return (*value) == obj;
	}
	inline bool operator==(std::nullptr_t null)
	{
		return value == nullptr;
	}
	inline T& $()noexcept { return *value; }
	inline T* operator->()noexcept
	{
		return value.get();
	}
	inline size_t addr()
	{
		return (size_t)value.get();
	}
	//use only $ref() function
	inline void __private__setter(std::shared_ptr<T> t)
	{
		value = t;
	}
};

#ifdef DEF_ANY
using std::any;

class dynamic :public Ref<any>
{
	template<typename T>
	dynamic(T obj)
	{
		auto o = std::any(obj);
		this->value = make_shared(o);
	}
	dynamic(dynamic d)
	{
		this->w_value = d.value;\\
	}
	dynamic() {}
	~dynamic() {}
	template<typename T>
	T* operator->()
	{
		return std::any_cast<T*>(value.get());
	}
	template<typename T>
	operator T& ()
	{
		return std::any_cast<T>(*value);
	}
	bool is_type(std::type_info t)
	{
		return type() == t;
	}

};

inline dynamic $ref() { return dynamic(nullptr); }
#endif


template<typename T>
inline Ref<T> $ref(T t) { return Ref<T>(t); }

template<typename T1, typename...T2>
inline Ref<T1> $ref(T2... t)
{
	Ref<T1> obj;
	obj.__private__setter(std::make_shared<T1>(t...));
	return obj;
}

class Vector3
{
public:
	static const int first = 0;
	static const int second = 1;
	static const int third = 2;
	static const int len = 3;
	Vector3();
	Vector3(json11::Json& item);
	Vector3(const Vector3& pos);
	Vector3(float x, float y, float z);
	ALfloat* toFloat();
	std::unique_ptr<ALfloat[]> toConstFloat()const;
	void set(float x, float y, float z);
	~Vector3() {};
	Vector3& operator=(const Vector3& vec);
	const bool operator==(const Vector3& vec)const;
	void Print(const std::string c="")const;
	float x, y, z;
	static const Vector3 none;
	static const void ptr const null;
private:
	ALfloat flt[len];
};

class Matrix3x3
{
	using vec_t=Vector3;
	using rc_vec_t = const vec_t&;
public:	
	static const int first = 0;
	static const int second = 1;
	static const int third = 2;
	static const int len = 3;
	Matrix3x3();
	Matrix3x3(json11::Json& item);
	Matrix3x3(const Matrix3x3& pos);
	Matrix3x3(vec_t x, vec_t y, vec_t z);
	void set(rc_vec_t _x, rc_vec_t _y, rc_vec_t _z);
	std::array<ALfloat*, 2> toFloat();
	std::array<std::unique_ptr<ALfloat[]>,2> toConstFloat()const;
	~Matrix3x3() {};
	const bool operator==(const Matrix3x3& mat)const;
	Matrix3x3& operator=(const Matrix3x3& vec);
	void Print(const std::string c="")const;
	vec_t x, y, z;
	static const Matrix3x3 none;
	static const void ptr const null;
private:
	ALfloat pos[len];
	ALfloat orient[len*2];
};
class FfmpgConverter;

typedef struct PacketQueue {
	AVPacketList *pFirstPkt;
	AVPacketList *pLastPkt;
	int numOfPackets;

	static void release(AVPacket& pPkt){av_free_packet(&pPkt);}

	PacketQueue& init(FfmpgConverter& cvt);

	inline bool isLast(){return this->pLastPkt;}

	int pushPacket(AVPacket *pPkt);

	int popPacket(AVPacket& pPkt);

	PacketQueue& destroy();

	//destoroy all objects
	PacketQueue& purge();

} PacketQueue;

class FfmpegObjector
{
public:
	FfmpegObjector(FfmpgConverter& cvt);
	~FfmpegObjector();
	FfmpegObjector& init();
	inline bool isInit(){return swr;}
	inline void initError(){ ScrOut::WriteLine("Could not allocate resampler context");}
	inline bool swrLenIsNone();
	inline enum AVSampleFormat* getDstAdrFmt(){return &dstSampleFmt;}
	inline void contextError(){ ScrOut::WriteLine("Failed to initialize the resampling context");}
	inline int getDstRate(){return dstRate;}
	inline enum AVSampleFormat getDstSampleFmt(){return dstSampleFmt;}
	inline int getDstNbChannels(){return dstNbChannels;}
	inline int64_t getDstChLayout(){return dstChLayout;}
	inline struct SwrContext* getSwr(){return swr;}
private:
	FfmpgConverter& converter;
	int dstRate;
	int dstNbChannels;
	int64_t dstChLayout;
	enum AVSampleFormat dstSampleFmt;
	struct SwrContext* swr;
};

class FfmpgIniter
{
public:
	FfmpgIniter();
	inline AVCodecContext* getcodecContext(){return codecContext;}
	~FfmpgIniter();
	inline void fileError(){ ScrOut::WriteLine("Error file is none");}
	inline bool isContextInit(const char* dir){return avformat_open_input(&formatContext, dir, NULL, NULL) != 0;}
	inline void contextError(){ ScrOut::WriteLine("Error opening the file");}
	inline bool isContextFind(){return avformat_find_stream_info(formatContext, NULL) < 0;}
	inline void finderError() { ScrOut::WriteLine("Error finding the stream info"); }
	inline bool airLenIsNone(){
		streamIndex = av_find_best_stream(formatContext, AVMEDIA_TYPE_AUDIO, -1, -1, &cdc, 0);
		return streamIndex < 0;
	}
	inline void airInitError() { ScrOut::WriteLine("Could not find any audio stream in the file"); }
	inline bool isCodecInit(){
		codecInit();
		return avcodec_open2(codecContext, codecContext->codec, NULL) != 0;
	}
	inline void codecError() { ScrOut::WriteLine("Couldn't open the context with the decoder"); }
	void settingOk();
	inline int airLength(){return audioStream->index;}
	inline AVFormatContext* getContext(){return formatContext;}
private:
	void codecInit();
	AVFormatContext* formatContext;
	AVCodecContext* codecContext;
	AVStream* audioStream;
	AVCodec* cdc;
	int streamIndex;
};

class FfmpgConverter
{
public:
	FfmpgConverter();
	~FfmpgConverter();
	FfmpgIniter& getIniter();
	FfmpegObjector& getObjecter();
	int decode(uint8_t* buf, int bufSize, AVPacket* packet);
	operator const bool()const{initer==nullptr&&objecter==nullptr?false:true;}
private:
	int decode(uint8_t* buf, int bufSize, AVPacket* packet, AVCodecContext* codecContext,
		SwrContext *swr, int dstRate, int dstNbChannels, enum AVSampleFormat* dstSampleFmt);
	FfmpgIniter* initer;
	FfmpegObjector* objecter;
};

abstract class BasicBinaural
{
public:
	using bb_obj = std::unordered_map < std::string, std::function<void()>>;
	using bbp_obj = std::unordered_map < std::string, std::function<void*()>>;
	BasicBinaural() 
		: funcs()
		, funcptrs()
		, pos()
		, rate()
		, use_ptr(nullptr)
		, bufstat_pt()
		, srcstat_pt()
	{};
	bb_obj funcs;
	bbp_obj funcptrs;
	Matrix3x3 pos;
	Vector3 rate;
	ALuint* use_ptr=nullptr;
	std::tuple<ALuint*, int> bufstat_pt;
	std::tuple<ALuint*, int> srcstat_pt;
};

abstract class baseAudio
{
	const int checkError(std::string& err,const int var)const;
public:
	abstract baseAudio(int argc, char* argv[]);
	abstract baseAudio();
	abstract ~baseAudio();
	void Release();
	static const int NUM_BUFFERS = 3;
	static const int NUM_SOURCES = 3;
	static const int DEFFAULT_SAMPLINGRATE = 44100;
	abstract virtual void Init(){};
	abstract virtual void Play(){};
	abstract virtual inline void SetLoop(){}
	abstract virtual void Play3D(Matrix3x3& pos){};
	abstract virtual void pauseAndRestart(){}
	inline const bool IsError()const{return alGetError() != AL_NO_ERROR;}
	inline const bool IsError(std::string& err)const { return checkError(err, alGetError())!= AL_NO_ERROR; }
	abstract virtual inline void Play3D(Ref<Matrix3x3> pos){Play3D(pos.$());}
	abstract virtual const bool Load(const std::string& dir){ ScrOut::WriteLine("object error");return false;};
	inline ALCcontext* getContext(){return m_context;}
	inline ALCdevice* getDevice(){return device;}
	abstract virtual inline ALuint* getBuffers(){return nullptr;}
	abstract virtual inline ALuint* getSources(){return nullptr;}
	inline FfmpgConverter* getConverter(){return &converter;}
	abstract virtual int bufLen(){return 0;}
	abstract virtual void SetVolume(long size){}
	abstract virtual void Looping(long arg){}
	abstract virtual void SetVelcity(Vector3& vel){}
	abstract virtual void SetPosition(Matrix3x3& pos){}
	abstract virtual const bool Error(std::string error_name) { return false; }
	abstract virtual void makeObjector() {};
	static std::string toHex(const int var)
	{
		std::stringstream ss;
		ss << std::hex << var << std::dec;
		auto s = ss.str();
		for (auto& ns : s) {
			if (ns >= 97 && ns <= 122) {
				ns -= 32;
			}
		}
		return s;
	}
	Property<BasicBinaural::bb_obj> funcs;
	Property<BasicBinaural::bbp_obj> funcptr;
	Property<Matrix3x3> pos;
	Property<Vector3> rate;
protected:
	FfmpgConverter converter;
	ALCdevice* device;
	ALCcontext* m_context;
	int len;
	bool is_pause;
};

class testAudio:public baseAudio
{
	using super = baseAudio;
public:
	testAudio(int argc, char* argv[]);
	testAudio();
	~testAudio();
	void Init()override;
	void Play()override;
	inline void Play3D(Ref<Matrix3x3> pos)override{Play3D(pos.$());}
	inline ALuint* getBuffers()override{return &buffer;}
	inline ALuint* getSources()override{return &source;}
	inline void SetLoop()override{alSourcei(source, AL_LOOPING, AL_TRUE);}
	int bufLen()override{return sizeof(&buf_data[0]);}
private:
	int fmt;
	int size;
	bool in_value;
	std::unique_ptr<signed short[]> buf_data;
	ALuint buffer;
	ALuint source;
};

class WavAudio:public baseAudio
{
	using super = baseAudio;
	BasicBinaural obj_3d;
public:
	WavAudio(int argc, char* argv[]);
	WavAudio();
	~WavAudio();
	void makeObjector()override;
	const bool Load(const std::string& dir)override;
	void Play()override;
	void Play3D(Matrix3x3& pos)override;
	inline void Play3D(Ref<Matrix3x3> pos)override{Play3D(pos.$());}
	inline ALuint* getBuffers()override{return &buffer;}
	inline ALuint* getSources()override{return &source;}
	inline void SetLoop()override{alSourcei(source, AL_LOOPING, AL_TRUE);}
	int bufLen()override{return sizeof(&buf_data[0]);}
private:
	int fmt;
	int size;
	bool in_value;
	std::unique_ptr<signed short[]> buf_data;
	std::string dir;
	ALuint buffer;
	ALuint source;
};

class Mp3Audio:public baseAudio
{
	BasicBinaural obj_3d;
	using super = baseAudio;
public:
	Mp3Audio(int argc, char* argv[]);
	Mp3Audio();
	~Mp3Audio();
	const bool Load(const std::string& dir)override;
	void Play()override;
	void Play3D(Matrix3x3& pos)override;
	inline void Play3D(Ref<Matrix3x3> pos)override{Play3D(pos.$());}
	inline ALCcontext* getContext(){return m_context;}
	inline ALCdevice* getDevice(){return device;}
	inline ALuint* getBuffers(){return buffer;}
	inline ALuint* getSources()override{return sources;}
	void pauseAndRestart()override;
	void each();
	inline void SetLoop()override{alSourcei(sources[0], AL_LOOPING, AL_TRUE);}
	int bufLen()override{return sizeof(&buf_data[0]);}
	void SetVolume(long size)override;
	void Looping(long arg)override;
	void SetVelcity(Vector3& vel)override;
	void SetPosition(Matrix3x3& pos)override;
	const bool Error(std::string error_name)override;
	void makeObjector()override;
	ALuint Num(void* ret) { return reinterpret_cast<ALuint>(ret); }
private:
	int fmt;
	std::unique_ptr<std::uint8_t[]> buf_data;
	int size;
	bool in_value;
	std::unique_ptr<PacketQueue> pkt_queue;
	ALuint buffer[super::NUM_BUFFERS];
	ALuint sources[super::NUM_SOURCES];
	std::string dir;
};

class binauralLib
{
	using prop0 = std::string;
public:
	binauralLib();
	binauralLib(const binauralLib& value);
	~binauralLib() {};
	void Play();
	void Play3D(Matrix3x3& pos);
	inline void Play3D(Ref<Matrix3x3> pos) { Play3D(pos.$()); }
	binauralLib clone();
	std::function<bool()> Init(std::string dir = "");
	void SetVolume(long size);
	void Looping(long arg = 1);
	void SetVelcity(Vector3& vel);
	Vector3 Velcity(Vector3 vel) { this->SetVelcity(vel); return vel; }
	void SetPosition(Matrix3x3& pos);
	baseAudio* of() { return m_audio.get(); }
private:
	std::unique_ptr<abstract baseAudio> m_audio;
public:
};

template<size_t len>
class CheckString
{
	class has
	{
	public:
		template<typename T>
		static constexpr size_t length = sizeof(T);

		has(FILE*& fp)
			:fp(fp)
			, type{ '\0' }
			, is_dat(true)
		{}
		~has() {}
		has& read(const char a)
		{
			fread(type, length<char>, 1, fp);
			if (type[0] == a)
			{
				is_dat &= true;
				return *this;
			}
			is_dat = false;
			return *this;
		}
		inline const bool read(const std::string a)
		{
			for (const auto f : a)
			{
				this->read(f);
			}
			return is_dat;
		}
	private:
		FILE*& fp;
		char type[1];
		bool is_dat;
	};
public:
	template<typename T>
	static constexpr size_t length = sizeof(T);
	CheckString(const char* dir)
	:type{}
	,fp(nullptr)
	{
		fp = fopen(dir, "rb");
	};
	~CheckString(){
		if(fp){
			fclose(fp);
		}
	};
	template<typename T>
	CheckString& read(T& size)
	{
		fread(&size, length<T>, 1, fp);$ret
	}

	CheckString& read()
	{
		fread(type, length<char>, len, fp);
		Putln<int>(std::to_string(length<char>));
		$ret
	}

	inline const bool operator!=(const char* $2){
		bool ret = false;
		for (size_t i = 0; i < len; i++){ret|=(type[i] != $2[i]);}
		return ret;
	}

	const bool isNone()
	{
		if(fp == nullptr){return true;}
		else{return false;}
	}

	FILE*& file(){return fp;}

	const bool isData()
	{
		int i = 0;
		for (; i < 256; i++) {
			if (has(fp).read("data"))
			{
				return true;
			}
		}
		ScrOut::WriteLine("No data!!!");
		return false;
	}

	CheckString& WriteLine()
	{
		char out[50];
		for (size_t i = 0; feof(fp)==0; i++)
		{
			fgets(out, sizeof(out),fp);
			ScrLog::WriteLine(out);
		}
		$ret;
	}

private:
	FILE* fp;
	char type[len];
};

class Path
{
#if IS_WIN

	static const bool __isHavingCheck(pch dir)
	{
		return PathFileExistsA(dir);
	}
#else
	static const bool __isHavingCheck(pch path)
	{
		struct stat st;
		if (stat(path, &st) != 0) {
			return false;
		}
		return (bool)S_ISREG(st.st_mode);
	}

#endif

public:
	static std::wstring toWide(pch mb)
	{
		wchar_t* w=nullptr;
		mbstowcs(w, mb, strlen(mb) + 1);
		return w;
	}
	static std::string toMulti(const wchar_t* w)
	{
		char* mb = nullptr;
		wcstombs(mb, w, wcslen(w) + 1);
		return mb;
	}
	static std::wstring toWide(const std::string& mb)
	{
		wchar_t* w = nullptr;
		mbstowcs(w, mb.c_str(), mb.size() + 1);
		return w;
	}
	static std::string toMulti(const std::wstring& w)
	{
		char* mb = nullptr;
		wcstombs(mb, w.c_str(), w.size() + 1);
		return mb;
	}
	inline static std::string toMulti(const char* w)
	{
		return w;
	}
private:
#ifdef UNICODE
	using char_t = wchar_t;
	using str_t = std::wstring;
#else
	using char_t = char;
	using str_t = std::string;
#endif
	#if defined(IS_WIN)
	std::string __getFullPath()
		const
	{
		char_t szFullPath[MAX_PATH] = { '\0' };
		char_t* szFilePart;
		GetFullPathName(
#ifdef UNICODE
			toWide(dir).c_str(), /* ファイル名を相対パスで指定 */		
#else
			dir,/* ファイル名を相対パスで指定 */
#endif
			sizeof(szFullPath) / sizeof(szFullPath[0]),
			szFullPath,
			& szFilePart);
		return toMulti(szFullPath);
	}
	#elif defined(IS_POSIX)
	void lntrim(char *str)
		const
	{  
		int i = 0;  
		while(1) {  
		    if(str[i] == '\n') {  
				str[i] = '\0';  
				break;  
			}  
			i++; 
		} 
	}  

	std::string __getFullPath()
		const
	{
		FILE* fp=popen((std::string() + "readlink -f " + dir).c_str(),"r");
		if (!fp) {return dir; }
		char buf[256];
		while (!feof(fp)) {
			fgets(buf, sizeof(buf), fp);
		}
		(void)pclose(fp);
		lntrim(buf);
		return buf;
	}
	#endif
public:
	Path()
	{
		this->dir = "";
	}
	Path(pcch dir)
	{ 
		this->dir = If(dir) then dir other "";
	}
	Path(const std::string& dir) 
	{
		this->dir = dir.c_str();
	}
	std::string getExt()
		const
	{
		auto a=strrchr(dir,'.');
		return If(a) then a other "";
	}
#ifdef IS_WIN
	inline const bool isPathRooted()
		const
	{
		return If(strchr(dir, ':')) then true other false;
	}
#else
	inline const bool isPathRooted()
		const
	{
		return *(dir+0) == '/' then true other false;
	}
#endif 
	const bool isExt(pch c)
		const
	{
		return getExt()==c;
	}
	std::string full_path()
	{
		return __getFullPath();
	}
	inline const bool isHaving()
	{
		return __isHavingCheck(If(isPathRooted()) then dir other __getFullPath().c_str());
	}
	inline const bool isNotHaving() { return !isHaving(); }
private:
	pch dir;
};

class OS
{
public:
	OS();
	~OS() {};
	inline pch getAppCommand()
	{
		return __installer;
	}
	inline const bool ofGetApp(pch app)
		const
	{
	#if __linux__
		system((std::string("sudo ") + __installer + " install " + app).c_str());
	#else
		system((std::string() + __installer + " install " + app).c_str());
	#endif 
	}
	inline const bool ofGetApp(const std::string& app)
		const
	{
		ofGetApp(app.c_str());
	}
#ifdef IS_WIN
	inline const bool getApp(pch app)
		const
	{
		std::string dir("C:\\ProgramData\\chocolatey\\bin");
		dir += app; dir += ".exe"; Path path(dir);
		return path.isHaving() then true other ofGetApp(dir);
	}
#endif
private:
	pch __installer="";
public:
};

#if IS_POSIX
class audioHelloLib
{
public:
	audioHelloLib() {}
	void alFunc();
	void alutFunc(int argc, char* argv[]);
	void Print();
private:
};

#endif
