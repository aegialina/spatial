#include "audio.hpp"
#ifndef AVCODEC_MAX_AUDIO_FRAME_SIZE
#define AVCODEC_MAX_AUDIO_FRAME_SIZE 192000
#endif
const Vector3 Vector3::none = Vector3(0, 0, 0);
const Matrix3x3 Matrix3x3::none = Matrix3x3(Vector3::none, Vector3::none, Vector3::none);
const void ptr const Vector3::null = &Vector3::none;
const void ptr const Matrix3x3::null = &Matrix3x3::none;

PacketQueue& PacketQueue::init(FfmpgConverter& cvt)
{
	memset(this, 0, sizeof(self_t));

	AVPacket readingPacket;
	av_init_packet(&readingPacket);

	auto& initer = cvt.getIniter();
	while (av_read_frame(initer.getContext(), &readingPacket) >= 0)
	{
		// Is this a packet from the video stream?
		if (readingPacket.stream_index == initer.airLength())
		{
			pushPacket(&readingPacket);
		}
		else
		{
			av_free_packet(&readingPacket);
		}
	}$ret
}

PacketQueue& PacketQueue::destroy()
{
	AVPacket decodingPacket;
	if (popPacket(decodingPacket) < 0)
    	$ret
  	av_free_packet(&decodingPacket);
	$ret
}

//destoroy all packets
PacketQueue& PacketQueue::purge()
{
	// パケットを解放する。
  	while (numOfPackets) {
    	AVPacket decodingPacket;
    	if (popPacket(decodingPacket) < 0)
      		continue;
    	av_free_packet(&decodingPacket);
  	}$ret
}

inline AVPacketList ptr pListMalloc()
{
	return (AVPacketList ptr)av_malloc(sizeof(AVPacketList));
}

int PacketQueue::pushPacket(AVPacket *pPkt)
{

	if (av_dup_packet(pPkt) < 0) {
    	return -1;
	}
	
	auto pPktList=pListMalloc();
	
	if (!pPktList) {
    	return -1;
	}

	pPktList->pkt = *pPkt;
	pPktList->next = NULL;
  
  	//
	if (!this->isLast()) {
    	this->pFirstPkt = pPktList;
  	}
	else {
    	this->pLastPkt->next = pPktList;
  	}
  	this->pLastPkt = pPktList;
  	//
  	this->numOfPackets++;

  	return 0;
}

int PacketQueue::popPacket(AVPacket& pPkt)
{
	AVPacketList *pPktList;
  
    //
	pPktList = this->pFirstPkt;
  
  	if (pPktList)
	{
    	this->pFirstPkt = pPktList->next;
    	if (!this->pFirstPkt) {
      		this->pLastPkt = NULL;
    	}
    	this->numOfPackets--;
    	pPkt = pPktList->pkt;
    	av_free(pPktList);
    	return 0;
  	}
 	return EOF;
}


FfmpegObjector::FfmpegObjector(FfmpgConverter& cvt)
    : converter(cvt)
    , dstRate(0)
    , dstNbChannels(0)
	, dstChLayout(0)
	, dstSampleFmt()
	, swr(nullptr)
{
}

FfmpegObjector::~FfmpegObjector()
{
    if (swr)
    {
        swr_free(&swr);
    }
}

FfmpegObjector& FfmpegObjector::init()
{
    // Set up SWR context once you've got codec information
    dstRate = converter.getIniter().getcodecContext()->sample_rate;
    dstChLayout = AV_CH_LAYOUT_STEREO;
    dstSampleFmt = AV_SAMPLE_FMT_S16;
    // buffer is going to be directly written to a rawaudio file, no alignment
    dstNbChannels = av_get_channel_layout_nb_channels(dstChLayout);
    swr = swr_alloc();
	$ret
}

bool FfmpegObjector::swrLenIsNone() {
    // チャンネルレイアウトがわからない場合は、チャンネル数から取得する。
    auto&& cdc = converter.getIniter().getcodecContext();
    if (cdc->channel_layout == 0)
        cdc->channel_layout = av_get_default_channel_layout(cdc->channels);
    //
    av_opt_set_int(swr, "in_channel_layout", cdc->channel_layout, 0);
    av_opt_set_int(swr, "out_channel_layout", dstChLayout, 0);
    av_opt_set_int(swr, "in_sample_rate", cdc->sample_rate, 0);
    av_opt_set_int(swr, "out_sample_rate", dstRate, 0);
    av_opt_set_sample_fmt(swr, "in_sample_fmt", cdc->sample_fmt, 0);
    av_opt_set_sample_fmt(swr, "out_sample_fmt", dstSampleFmt, 0);
    return swr_init(swr) < 0;
}

FfmpgIniter::FfmpgIniter()
    : formatContext(nullptr)
    , cdc(nullptr)
    , codecContext(nullptr)
    , audioStream(nullptr)
	, streamIndex()
{
    // FFmpegのログレベルをQUIETに設定する。（何も出力しない）
    av_log_set_level(AV_LOG_QUIET);

    // FFmpegを初期化
    av_register_all();
}

void FfmpgIniter::settingOk()
{
	ScrOut::WriteLine("This stream has ",codecContext->channels
        ," channels and a sample rate of ",codecContext->sample_rate,"Hz");
	ScrOut::WriteLine("The data is in the format ",av_get_sample_fmt_name(codecContext->sample_fmt));
}

FfmpgIniter::~FfmpgIniter()
{
    if (codecContext) { avcodec_close(codecContext); }
    if (formatContext) { avformat_close_input(&formatContext); }
    if (formatContext) { avformat_free_context(formatContext); }
}

void FfmpgIniter::codecInit()
{
    audioStream = formatContext->streams[streamIndex];
    codecContext = audioStream->codec;
    codecContext->codec = cdc;
}

FfmpgConverter::FfmpgConverter()
    :initer(nullptr)
    ,objecter(nullptr)
{}

FfmpgConverter::~FfmpgConverter()
{
    if (initer) { delete initer; }
    if (objecter) { delete objecter; }
}

FfmpgIniter& FfmpgConverter::getIniter()
{
    if (initer)return *initer;
    initer = new FfmpgIniter();
    return *initer;
}

FfmpegObjector& FfmpgConverter::getObjecter()
{
    if (objecter)return *objecter;
    objecter = new FfmpegObjector(*this);
    return *objecter;
}

int FfmpgConverter::decode(uint8_t* buf, int bufSize, AVPacket* packet, AVCodecContext* codecContext,
    SwrContext* swr, int dstRate, int dstNbChannels, enum AVSampleFormat* dstSampleFmt)
{

    unsigned int bufIndex = 0;
    unsigned int dataSize = 0;

    AVFrame* frame = av_frame_alloc();
    if (!frame) {
		ScrOut::WriteLine("Error allocating the frame");
        av_free_packet(packet);
        return 0;
    }


    while (packet->size > 0)
    {
        // デコードする。
        int gotFrame = 0;
        int result = avcodec_decode_audio4(codecContext, frame, &gotFrame, packet);

        if (result >= 0 && gotFrame)
        {

            packet->size -= result;
            packet->data += result;

            // 変換後のデータサイズを計算する。
            int dstNbSamples = (int)av_rescale_rnd(frame->nb_samples, dstRate, codecContext->sample_rate, AV_ROUND_UP);
            uint8_t** dstData = NULL;
            int dstLineSize;
            if (av_samples_alloc_array_and_samples(&dstData, &dstLineSize, dstNbChannels, dstNbSamples, *dstSampleFmt, 0) < 0) {
				ScrOut::WriteLine("Could not allocate destination samples");
                dataSize = 0;
                break;
            }

            // デコードしたデータをSwrContextに指定したフォーマットに変換する。
            int ret = swr_convert(swr, dstData, dstNbSamples, (const uint8_t**)frame->extended_data, frame->nb_samples);
            //int ret = swr_convert(swr, dstData, *dstNbSamples, (const uint8_t **)frame->data, frame->nb_samples);
            if (ret < 0) {
				ScrOut::WriteLine("Error while converting");
                dataSize = 0;
                break;
            }

            // 変換したデータのサイズを計算する。
            int dstBufSize = av_samples_get_buffer_size(&dstLineSize, dstNbChannels, ret, *dstSampleFmt, 1);
            if (dstBufSize < 0) {
				ScrOut::WriteLine("Error av_samples_get_buffer_size()");
                dataSize = 0;
                break;
            }
			
            if ((int)dataSize + dstBufSize > bufSize) {
				ScrOut::WriteLine("dataSize + dstBufSize > bufSize");
                dataSize = 0;
                break;
            }

            // 変換したデータをバッファに書き込む
            memcpy((uint8_t*)buf + bufIndex, dstData[0], dstBufSize);
            bufIndex += dstBufSize;
            dataSize += dstBufSize;

            if (dstData)
                av_freep(&dstData[0]);
            av_freep(&dstData);

        }
        else {

            packet->size = 0;
            packet->data = NULL;

        }
    }

    av_free_packet(packet);
    av_free(frame);

    return dataSize;
}

int FfmpgConverter::decode(uint8_t* buf, int bufSize, AVPacket* packet)
{
    return decode(buf, bufSize, packet,
        initer->getcodecContext(), objecter->getSwr(), objecter->getDstRate(),
        objecter->getDstNbChannels(), objecter->getDstAdrFmt());
}

Vector3::Vector3()
	:x(0)
	,y(0)
	,z(0)
	,flt()
{
}

Vector3::Vector3(float _x,float _y,float _z)
	:x(_x)
	,y(_y)
	,z(_z)
	,flt()
{}

Vector3::Vector3(const Vector3& pos)
	:x(pos.x)
	,y(pos.y)
	,z(pos.z)
	,flt()
{
}

void Vector3::set(float _x, float _y, float _z)
{
	x = _x;
	y = _y;
	z = _z;
}

void Matrix3x3::set(rc_vec_t _x, rc_vec_t _y, rc_vec_t _z)
{
	x = _x;
	y = _y;
	z = _z;
}

const bool Vector3::operator==(const Vector3& vec)
	const
{
	return x == vec.x && y == vec.y && z == vec.z;
}

Vector3& Vector3::operator=(const Vector3& vec)
{
	x = vec.x;
	y = vec.y;
	z = vec.z;$ret
}

const bool Matrix3x3::operator==(const Matrix3x3& mat)
	const
{
	return x == mat.x && y == mat.y && z == mat.z;
}

Matrix3x3& Matrix3x3::operator=(const Matrix3x3& vec)
{
	x = vec.x;
	y = vec.y;
	z = vec.z;$ret
}

void Vector3::Print(std::string c)
	const
{
	if(c != ""){ScrOut::Write(c,":");}
	ScrOut::WriteLine("{x: ", x, ",y: ", y, ",z: ", z, "}");
}

Matrix3x3::Matrix3x3()
	:x()
	,y()
	,z()
	,pos()
	,orient()
{
}

Vector3::Vector3(json11::Json& item)
	:x()
	,y()
	,z()
	,flt()
{

	json11::Json::array objects;
	if (item.is_array())
	{
		objects = item.array_items();
	}
	else
	{				
		ScrOut::WriteLine("Error1:typeError");
		goto Error;	
	}
	{
		float flts[3] = {0,0,0};
		for (std::uint16_t i = 0; i < 3; i++)
		{
			if(objects.size()==i)
			{
				ScrOut::WriteLine("Error1:sizeError");
				goto Error;	
			}
			auto& object = objects[i];
			auto& flt = flts[i];
			if (object.is_number())
			{
				flt=(float)object.number_value();
			}
			else
			{
			flt=0;
			}
		}
		x=flts[0];
		y=flts[1];
		z=flts[2];
	}
	Error:
	return;
}

Matrix3x3::Matrix3x3(json11::Json& item)
	:x(Vector3::none)
	,y(Vector3::none)
	,z(Vector3::none)
	,pos()
	,orient()
{
	json11::Json::array objects;
	if(item.is_array()){
		objects= item.array_items();
	}
	else
	{
		ScrOut::WriteLine("Error0:typeError");
		goto Error;
	}
	{
		Vector3 vset[3];
		for (std::uint16_t i = 0; i < 3; i++)
		{
			if(objects.size()==i)
			{
				ScrOut::WriteLine("Error0:sizeError");
				goto Error;	
			}
			vset[i]=Vector3(objects[i]);
		}
		x = vset[0];
		y = vset[1];
		z = vset[2];
	}
	Error:
	return;
}

Matrix3x3::Matrix3x3(vec_t _x, vec_t _y, vec_t _z)
	: x(_x)
	, y(_y)
	, z(_z)
	, pos()
	, orient()
{}

Matrix3x3::Matrix3x3(const Matrix3x3& mat)
	: x(mat.x)
	, y(mat.y)
	, z(mat.z)
	, pos()
	, orient()
{
}

std::unique_ptr<ALfloat[]> Vector3::toConstFloat() 
	const
{
	auto flt=std::make_unique<ALfloat[]>(3);
	flt[0]=x;
	flt[1]=y;
	flt[2]=z;
	return flt;
}

ALfloat* Vector3::toFloat()
{
	flt[0]=x;
	flt[1]=y;
	flt[2]=z;
	return flt;
}

std::array<std::unique_ptr<ALfloat[]>,2> Matrix3x3::toConstFloat() 
	const
{
	std::array<std::unique_ptr<ALfloat[]>,2> arr;
	auto&pos=arr[0]=std::make_unique<ALfloat[]>(3);
	auto&orient=arr[1]=std::make_unique<ALfloat[]>(6);
	pos[0]=x.x;
	pos[1]=x.y;
	pos[2]=x.z;
	orient[0]=y.x;
	orient[1]=y.y;
	orient[2]=y.z;
	orient[3]=z.x;
	orient[4]=z.y;
	orient[5]=z.z;
	return arr;
}

std::array<ALfloat*,2> Matrix3x3::toFloat()
{
	pos[0]=x.x;
	pos[1]=x.y;
	pos[2]=x.z;
	orient[0]=y.x;
	orient[1]=y.y;
	orient[2]=y.z;
	orient[3]=z.x;
	orient[4]=z.y;
	orient[5]=z.z;
	std::array<ALfloat*,2> arr{};
	arr[0]=pos;
	arr[1]=orient;
	return arr;
}


void Matrix3x3::Print(const std::string tag)
	const
{
	if(tag != ""){ScrOut::Write(tag,":");}
	ScrOut::WriteLine("{");
	ScrOut::Write(tab, "x:");
	x.Print();
	ScrOut::Write(tab, "y:");
	y.Print();
	ScrOut::Write(tab, "z:");
	z.Print();
	ScrOut::WriteLine("}");
}

binauralLib::binauralLib()
	:m_audio(nullptr)
{
}

binauralLib::binauralLib(const binauralLib& value)
	:m_audio(nullptr)
{
}

binauralLib binauralLib::clone()
{
	binauralLib al;
	al.m_audio=std::move(m_audio);
	m_audio=nullptr;
	return al;
}

void testAudio::Init()
{
	//unique_ptrで音源を入れる配列を動的確保
	buf_data=make_unique<signed short[]>(len);
    fmt=AL_FORMAT_MONO16;
    size=len * sizeof(signed short);
	//OpenALの下準備　おまじない的な
	device = alcOpenDevice(nullptr);
	m_context = alcCreateContext(device, nullptr);
	alcMakeContextCurrent(m_context);

	//それを生成
	alGenBuffers(1, &buffer);
	alGenSources(1, &source);
}

void baseAudio::Release()
{
	//OpenALの後始末
	if (alcGetCurrentContext())
	{
		alcMakeContextCurrent(nullptr);
	}
	if (m_context)
	{
		alcDestroyContext(m_context);
	}
	if (device)
	{
		alcCloseDevice(device);
	}
}

baseAudio::~baseAudio()
{
	Release();
}

WavAudio::~WavAudio()
{
	if(source)
	{
		alDeleteSources(1, &source);
	}
	if(buffer)
	{
		alDeleteBuffers(1, &buffer);
	}
	Release();
}

Mp3Audio::~Mp3Audio()
{
	pkt_queue->purge();
	is_pause=false;
	ScrOut::WriteLine("End.");
  	alDeleteSources(NUM_SOURCES,sources);
	alDeleteBuffers(NUM_BUFFERS, buffer);
	Release();
}

void WavAudio::Play() 
{
	//ここで440Hzのsin波を1秒間分生成
	if (buf_data) 
		return; 
	for (int i = 0; i < len; i++) 
	{
		buf_data[i] = (signed short)(32767 * sin(2 * M_PI * i * 440 / len));
	}
	//バッファに音源データを入れる
	alBufferData(buffer, fmt, &buf_data[0],size, converter.getObjecter().getDstRate());
	//ソースにバッファを適用
	alSourcei(source, AL_BUFFER, buffer);
	//ループ再生をON
	alSourcei(source, AL_LOOPING, AL_TRUE);
	//ソースを再生！
	alSourcePlay(source);
}

void testAudio::Play()
{
	for (int i = 0; i < len; i++) 
	{
		buf_data[i] = (signed short)(32767 * sin(2 * M_PI * i * 440 / len));
	}
	//バッファに音源データを入れる
	alBufferData(buffer, fmt, &buf_data[0],size, converter.getObjecter().getDstRate());
	//ソースにバッファを適用
	alSourcei(source, AL_BUFFER, buffer);
	//ループ再生をON
	alSourcei(source, AL_LOOPING, AL_TRUE);
	//ソースを再生！
	alSourcePlay(source);
}

class Eaches
{
public:
	Eaches()
	:decoding()
	{
	}
	operator const bool(){return decoding;}
	inline AVPacket& getDecoding(){return *decoding;}
	inline AVPacket* getDecPtr(){return decoding;}
	~Eaches()
	{
		if(*this)
		{
			PacketQueue::release(*decoding);
		}
	}
private:
	AVPacket* decoding;
};

void Mp3Audio::each()
{
	for (auto& buf:buffer) 
	{
    	// パケットキューにたまっているパケットを取得する。
    	AVPacket decodingPacket;
    	if (pkt_queue->popPacket(decodingPacket) < 0) {
			ScrOut::WriteLine("error.");
      		break;
    	}

		size=converter.decode(&buf_data[0], len, &decodingPacket);
		// デコード、変換したデータを、OpenALのバッファに書き込む。
		alBufferData(buf, fmt, &buf_data[0],size, converter.getObjecter().getDstRate());
    	if (IsError()) {
			ScrOut::WriteLine("Error:Buffer");
      		av_free_packet(&decodingPacket);
      		continue;
    	}    
    	av_free_packet(&decodingPacket);
  	}
}

baseAudio::baseAudio(int argc, char* argv[])
:converter()
,device(nullptr)
,m_context(nullptr)
,len(DEFFAULT_SAMPLINGRATE)
,is_pause(false)
{
	makeObjector();
}

baseAudio::baseAudio()
:converter()
,device(nullptr)
,m_context(nullptr)
,len(DEFFAULT_SAMPLINGRATE)
,is_pause(false)
{
	makeObjector();
}

const bool Mp3Audio::Load(const std::string& dir)
{
	std::ifstream file(dir);
	auto& initer=converter.getIniter();
	if(!file){
		initer.fileError();
		ScrOut::WriteLine("Checked:",dir);
		return false;
	}
	this->dir = dir;
	if (initer.isContextInit(dir.c_str())) {
    	initer.contextError();
    	return false;
  	}
  
  	if (initer.isContextFind()) {
    	initer.finderError();
    	return false;
 	}
	
	if (initer.airLenIsNone()) {
    	initer.airInitError();
    	return false;
  	}

  	if (initer.isCodecInit()) {
    	initer.codecError();
    	return false;
  	}

  	initer.settingOk();
  	auto &objecter=converter.getObjecter();

  	if (objecter.init().isInit() == false) {
    	objecter.initError();
    	return false;
	}

	if (objecter.swrLenIsNone()) {
    	objecter.contextError();
    	return false;
	}

  	//-----------------------------------------------------------------//
	device=alcOpenDevice(nullptr);
	if(!device){return false;}
	m_context=alcCreateContext(device, nullptr);
	alcMakeContextCurrent(m_context);
	if (obj_3d.pos != Matrix3x3::none)
	{
		funcs->at("init-position")();
	}
	if(!m_context){return false;}
	alGenBuffers(NUM_BUFFERS, buffer);
	alGenSources(NUM_SOURCES, sources);
	if(IsError()) {return false;}
	pkt_queue=make_unique<PacketQueue>();
	pkt_queue->init(converter);
	fmt=AL_FORMAT_STEREO16;
	len=AVCODEC_MAX_AUDIO_FRAME_SIZE;
	buf_data=std::make_unique<std::uint8_t[]>(len);
	each();
	return true;
}

void Mp3Audio::pauseAndRestart()
{
	if(is_pause)
	{
		ALint val;
		// もし再生が止まっていたら、再生する。
    	alGetSourcei(sources[0], AL_SOURCE_STATE, &val);
    	if (val != AL_PLAYING)
      		alSourcePlay(sources[0]);
    	// 掃除
	}
	else
	{
	}
	is_pause=!is_pause;
}

//0~100 Volume
void Mp3Audio::SetVolume(long size)
{
	float volume;
	if(size>100){volume=1.00f;}
	elif(size<0){
		volume=0.0f;
	}
	else
	{
		volume=(float)size;
		volume/=100;
	}
	alSourcei(sources[0], AL_GAIN , (ALint)volume);
}

//isLoop
void Mp3Audio::Looping(long arg)
{
	alSourcei(sources[0], AL_LOOPING, arg?AL_TRUE:AL_FALSE);
}

void Mp3Audio::SetVelcity(Vector3& vel)
{
	alListenerfv(AL_VELOCITY, vel.toFloat());
}

const int baseAudio::checkError(std::string& err, const int var)const 
{
	switch (var)
	{
	case AL_INVALID_NAME:
		err = "Exception(0x" + toHex(var) + ") is Invalid Name Error";
		break;
	case AL_INVALID_ENUM:
		err = "Exception(0x" + toHex(var) + ") is Invalid Number Error";
		break;
	case AL_INVALID_VALUE:
		err = "Exception(0x" + toHex(var) + ") is Argments Number Error";
		break;
	case AL_INVALID_OPERATION:
		err = "Exception(0x" + toHex(var) + ") is Invalid Operation Error";
		break;
	case AL_OUT_OF_MEMORY:
		err = "Exception(0x" + toHex(var) + ") is Memory Size Error";
		break;
	}
	return var;
}

void Mp3Audio::SetPosition(Matrix3x3& pos)
{
	auto flt = pos.toConstFloat();
	auto vel = Vector3::none.toConstFloat();
	[&]()
	{
		alListenerfv(AL_POSITION, flt[0].get());
		alListenerfv(AL_VELOCITY, vel.get());
		alListenerfv(AL_ORIENTATION, flt[1].get());
		pos.Print("Pos");
	}();

}

void Mp3Audio::makeObjector()
{
	auto initf = [](Matrix3x3* pos)
	{
		auto flt = pos->toConstFloat();
		alListenerfv(AL_POSITION, flt[0].get());
		alListenerfv(AL_VELOCITY, Vector3::none.toConstFloat().get());
		alListenerfv(AL_ORIENTATION, flt[1].get());
	};
	auto buff = [](Matrix3x3* pos,ALuint** buf)
	{
		auto flt = pos->toConstFloat();
		alBufferfv(**buf,AL_POSITION, flt[0].get());
		alBufferfv(**buf,AL_VELOCITY, Vector3::none.toConstFloat().get());
		alBufferfv(**buf,AL_ORIENTATION, flt[1].get());
	};
	auto srcf = [](Matrix3x3* pos, ALuint** buf)
	{
		auto flt = pos->toConstFloat();
		alSourcefv(**buf, AL_POSITION, flt[0].get());
		alSourcefv(**buf, AL_VELOCITY, Vector3::none.toConstFloat().get());
		alSourcefv(**buf, AL_ORIENTATION, flt[1].get());
	};
	auto src_stopf = [](ALuint** src)
	{
		if(*src)
		{
			alSourceStop(**src);
		}
	};
	auto src_restartf = [](ALuint** src)
	{
		if (*src)
		{
			alSourcePlay(**src);
		}
	};
	funcs = make_setter<BasicBinaural::bb_obj>(obj_3d.funcs);
	pos = make_setter<Matrix3x3>(obj_3d.pos);
	rate = make_setter<Vector3>(obj_3d.rate);
	funcptr = make_setter<BasicBinaural::bbp_obj>(obj_3d.funcptrs);
	obj_3d.funcptrs["self"] = std::bind([&]()->void* {return this; });
	obj_3d.funcptrs["use"] = std::bind([&]()->void* {return reinterpret_cast<void*>(*obj_3d.use_ptr); });
	obj_3d.funcs["init-position"] = std::bind(initf, &obj_3d.pos);
	obj_3d.funcs["buf-position"] = std::bind(buff, &obj_3d.pos, &obj_3d.use_ptr);
	obj_3d.funcs["src-position"] = std::bind(srcf, &obj_3d.pos, &obj_3d.use_ptr);
	obj_3d.funcs["src-stop"] = std::bind(src_stopf, &obj_3d.use_ptr);
	obj_3d.funcs["src-restart"] = std::bind(src_restartf, &obj_3d.use_ptr);
	obj_3d.bufstat_pt = std::make_tuple(&this->buffer[0],super::NUM_BUFFERS);
	obj_3d.bufstat_pt = std::make_tuple(&this->sources[0], super::NUM_SOURCES);
}

void Mp3Audio::Play3D(Matrix3x3& pos)
{
	obj_3d.funcs["position"] = std::bind([&](Matrix3x3* pos) {SetPosition(*pos); }, &pos);
	Play();
}

const bool Mp3Audio::Error(std::string error_name)
{
	std::string err;
	if (IsError(err))
	{
		ScrOut::WriteLine(err);
	#if IS_WIN
		Sleep(10000);
	#else
		sleep(10000);
	#endif
		return true;
	}
	return false;
}

void Mp3Audio::Play()
{
	// データが入ったバッファをキューに追加して、再生を開始する。
	if (funcs->count("position")) { obj_3d.funcs["position"](); }
	alSourceQueueBuffers(sources[0], NUM_BUFFERS, buffer);
  	alSourcePlay(sources[0]);
	if (Error("StartingError:")) { return; }
	else
	{
		std::string play_loc="Playing > " + dir;
		ScrOut::WriteLine(play_loc);
		is_pause=true;
  	}
	while(pkt_queue->numOfPackets)
	{
		ALint val;
    	alGetSourcei(sources[0], AL_BUFFERS_PROCESSED, &val);
    	if(val <= 0) {
      		// 少しスリープさせて処理を減らす。
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
      		continue;
    	}

    	AVPacket decodingPacket;
    	if (pkt_queue->popPacket(decodingPacket) < 0) {
			ScrOut::WriteLine("error.");
      		break;
   		}

	    size = converter.decode(buf_data.get(), len, &decodingPacket);

    	if (size <= 0) {continue;}

    	// 再生済みのバッファをデキューする。
    	ALuint of_buffer;
		obj_3d.use_ptr = &of_buffer;
    	alSourceUnqueueBuffers(sources[0], 1, obj_3d.use_ptr);
    	// デキューしたバッファに、新しい音楽データを書き込む。
		alBufferData(of_buffer, fmt, buf_data.get(),size, converter.getObjecter().getDstRate());
    
    	if(Error("Error Buffer :"))
    	{
      		return;
    	}	    
    
    	// 新しいデータを書き込んだバッファをキューする。
    	alSourceQueueBuffers(sources[0], 1, obj_3d.use_ptr);
		obj_3d.use_ptr = &sources[0];
		funcs->at("src-stop")(); 
		funcs->at("src-position")();
		funcs->at("src-restart")();
    	if (Error("Error buffering :"))
    	{
      		return;
    	}
		if(!is_pause)
		{
			pauseAndRestart();
		}
    	PacketQueue::release(decodingPacket);
  	}
}

const bool WavAudio::Load(const std::string& dir)
{
	unsigned long memory = 0, length = 0;
	this->dir=dir;
	//ファイルを開くときの変数
	auto cs=CheckString<4>(dir.c_str());
	if (cs.isNone()) {
		return false;
	}
	//cs.WriteLine();
	unsigned long size, chunkSize;
	short formatType, channels;
	unsigned long sampleRate, avgBytesPerSec;
	short bytesPerSample, bitsPerSample;
	unsigned long dataSize;
	//Check that the WAVE file is OK
	if (cs.read() != "RIFF") {
		ScrOut::WriteLine("not 'RIFF'");
		return false;
	}

	if (cs.read() != "WAVE") {
		ScrOut::WriteLine("not 'WAVE'");
		return false;
	}
	
	if (cs.read() != "fmt ") {
		ScrOut::WriteLine("not 'fmt '");
		return false;
	}
	
	//ここからchunkSizeバイト分がwavのパラメータ領域
	//基本的にpcm形式
	//wavのチャンネル数
	//サンプリング周波数
	cs.read(chunkSize).read(formatType).read(channels).read(sampleRate);
	//1秒間の平均転送レート(=channels*sampleRate*bitsPerSample / 8)
	//各サンプル数のbyte数(例えば、16bit, 2channelsなら4)
	//量子化ビット数　(16, 8)
	cs.read(avgBytesPerSec).read(bytesPerSample).read(bitsPerSample);
	if (cs.isData() == false) { return false; }
	//波形データ長
	cs.read(dataSize);
	//1サンプルの長さ
	memory = bitsPerSample / 8;
	//どのくらい読み込むか
	len = dataSize / memory;
	buf_data=make_unique<signed short[]>(len);
	fread(buf_data.get(), memory, length, cs.file());
	//OpenALの下準備　おまじない的な
	device = alcOpenDevice(nullptr);
	m_context = alcCreateContext(device, nullptr);
	alcMakeContextCurrent(m_context);

	//それを生成
	alGenBuffers(1, &buffer);
	alGenSources(1, &source);
	if (channels == 2) {
		for (uint32_t i = 0; i < length; i += 2) {
			//平均値をとる
			buf_data[i / 2] = buf_data[i] / 2 + buf_data[i + 1] / 2;
		}
	}
    fmt=AL_FORMAT_MONO16;
    size=len * sizeof(signed short);
	return true;
}

testAudio::testAudio(int argc, char* argv[])
:baseAudio(argc,argv)
,buffer()
,source()
,fmt()
,buf_data(nullptr)
,size()
,in_value(false)
{}

testAudio::testAudio()
:baseAudio()
,buffer()
,source()
,fmt()
,buf_data(nullptr)
,size()
,in_value(false)
{}

testAudio::~testAudio()
{
	// バッファの破棄
	if (buffer) 
	{
		alDeleteBuffers(1, &buffer);
	}
	// ソースの破棄
	if (source) 
	{
		alDeleteSources(1, &source);
	}
	Release();
}

WavAudio::WavAudio(int argc, char* argv[])
:baseAudio(argc,argv)
,buffer()
,source()
,dir()
,fmt()
,buf_data(nullptr)
,size()
,in_value(false)
{
	makeObjector();
}

void WavAudio::makeObjector()
{
	funcs = make_setter<BasicBinaural::bb_obj>(obj_3d.funcs);
	pos = make_setter<Matrix3x3>(obj_3d.pos);
	rate = make_setter<Vector3>(obj_3d.rate);
	funcptr = make_setter<BasicBinaural::bbp_obj>(obj_3d.funcptrs);
}

WavAudio::WavAudio()
:baseAudio()
,buffer()
,source()
,dir()
,fmt()
,buf_data(nullptr)
,size()
,in_value(false)
{
	makeObjector();
}

Mp3Audio::Mp3Audio(int argc, char* argv[])
:baseAudio(argc,argv)
,buffer{ALuint(),ALuint(),ALuint()}
,sources()
,dir()
,pkt_queue(nullptr)
,fmt()
,buf_data(nullptr)
,size()
,in_value(false)
{
	makeObjector();
}

Mp3Audio::Mp3Audio()
:baseAudio()
,buffer{ALuint(),ALuint(),ALuint()}
,sources()
,dir()
,pkt_queue(nullptr)
,fmt()
,buf_data(nullptr)
,size()
,in_value(false)
{
	makeObjector();
}

void WavAudio::Play3D(Matrix3x3& pos)
{
	auto flt=pos.toFloat();
	alListenerfv(AL_POSITION,flt[0]);
	alListenerfv(AL_ORIENTATION,flt[1]);
	Play();
}

void binauralLib::Play()
{
	if(m_audio)
	{
		m_audio->Play();
	}
}

void binauralLib::Play3D(Matrix3x3& pos)
{
	if(m_audio)
	{
		m_audio->Play3D(pos);
	}
}

void binauralLib::SetVolume(long size)
{
	Print("volume(%):", size);
	m_audio->SetVolume(size);
}

void binauralLib::Looping(long arg)
{
	m_audio->Looping(arg);
}

void binauralLib::SetVelcity(Vector3& vel)
{
	m_audio->SetVelcity(vel);
}

void binauralLib::SetPosition(Matrix3x3& pos)
{
	m_audio->SetPosition(pos);
}

std::function<bool()> binauralLib::Init(std::string dir)
{
	Path path(dir);
	if(path.getExt()==".mp3")
	{
		m_audio = make_unique<Mp3Audio>();
		return std::bind([&](const std::string dir) {return m_audio->Load(dir); }, dir);
	}
	elif(path.getExt()==".wav")
	{
		m_audio = make_unique<WavAudio>();
		return std::bind([&](const std::string dir) {return m_audio->Load(dir); }, dir);
	}
	else
	{
		m_audio = make_unique<testAudio>();
		return std::bind([&]() {m_audio->Init(); return true; });
	}
}

# if IS_POSIX
void audioHelloLib::alFunc()
{
    //OpenALの下準備　おまじない的な
	ALCdevice *device = alcOpenDevice(nullptr);
	ALCcontext *m_context = alcCreateContext(device, nullptr);
	alcMakeContextCurrent(m_context);
	
	//バッファ(保存領域)とソース(音源)を宣言
	ALuint buffer;
	ALuint source;
	//それを生成
	alGenBuffers(1, &buffer);
	alGenSources(1, &source);

	//unique_ptrで音源を入れる配列を動的確保
	auto buf_data = make_unique<signed short[]>(baseAudio::DEFFAULT_SAMPLINGRATE);
	//ここで440Hzのsin波を1秒間分生成
	for (int i = 0; i < baseAudio::DEFFAULT_SAMPLINGRATE; i++) {
		buf_data[i] = 32767 * sin(2 * M_PI*i * 440 / baseAudio::DEFFAULT_SAMPLINGRATE);
	}
    
	//バッファに音源データを入れる
	alBufferData(buffer, AL_FORMAT_MONO16, &buf_data[0], baseAudio::DEFFAULT_SAMPLINGRATE * sizeof(signed short), baseAudio::DEFFAULT_SAMPLINGRATE);
	//ソースにバッファを適用
	alSourcei(source, AL_BUFFER, buffer);
	//ループ再生をON
	alSourcei(source, AL_LOOPING, AL_TRUE);
	//ソースを再生！
	alSourcePlay(source);

	//ここで一時停止
	alutSleep( 1 );

	// バッファの破棄
	alDeleteBuffers(1, &buffer);
	// ソースの破棄
	alDeleteSources(1, &source);
	
	//OpenALの後始末
	alcMakeContextCurrent(nullptr);
	alcDestroyContext(m_context);
	alcCloseDevice(device);

}

void audioHelloLib::alutFunc(int argc, char* argv[])
{
    ALuint buffer, source;
    // ALUT の初期化を行ないます。
    alutInit( &argc, argv );

    // 有効なバッファとソース番号を取得します。
    alGenBuffers( 1, &buffer );
    alGenSources( 1, &source );

    // 用意されている「はろーわーるど」な音声のバッファを作ります。
    buffer = alutCreateBufferHelloWorld();
    // ソースとバッファを結びつけます。
    alSourcei( source, AL_BUFFER, buffer );

    // 音声を再生します。
    alSourcePlay( source );

    // 1 秒待ちます。
    alutSleep( 1 );
}

void audioHelloLib::Print()
{
	ScrOut::Write("Test");
}

#endif
#include "__app__.hpp"

void addJsonList(std::vector<json11::Json>& jsonobj,const char* arg)
{
	std::ifstream file(arg);
	if (!file) 
		return;
	std::string str(
		(std::istreambuf_iterator<char>(file)),
		std::istreambuf_iterator<char>());
	std::string out;
	jsonobj.push_back(json11::Json::parse(str,out));
	if(!(out==""))
	{
		ScrOut::WriteLine("Error:",out);
	}
}

inline void addJsonList(std::vector<json11::Json>& jsonobj,std::string arg)
{
	addJsonList(jsonobj,arg.c_str());
}

using striitrb = std::istreambuf_iterator<char>;

inline json11::Json getJson(std::string arg)
{	
	std::ifstream file(arg.c_str());
	if (!file) 
		return nullptr;
	auto str=std::string(striitrb(file),striitrb());
	std::string out;
	auto json=json11::Json::parse(str,out);
	if(out != "")
	{
		ScrOut::WriteLine("Error:",out);
		json=nullptr;
	}
	return json;
}

bool argmental(int length, char** arg, std::vector<json11::Json>& jsonobj)
{
	if (length >= 2)
	{
		for (int i = 1; length != i; i++)
		{
			Path path(*(arg+i));
			if (path.isExt(".json"))
			{
				addJsonList(jsonobj, *(arg+i));
			}
			elif(path.isExt(""))
			{
				std::string s = *(arg+i);
				auto j = getJson(s + ".json");
				if (j == nullptr)
				{
					ScrOut::WriteLine("NoFile:", path.full_path(), ".json", " is None");
					continue;
				}
				else
				{
					if (j.is_array() == false)
					{
						ScrOut::WriteLine("NoPlayLists:", path.full_path(), ".json", " is not PlayLists");
						continue;
					}
					for (auto& jo : j.array_items())
					{
						if (jo.is_string())
						{
							addJsonList(jsonobj, jo.string_value() + ".json");
						}
						else
						{
							continue;
						}
					}
				}
			}
			else
			{
				ScrOut::WriteLine("Error:", path.full_path(), " is not json.");
			}
		}
		return false;
	}
	else
	{
		auto ahls = $ref<binauralLib>();
		std::string s;
		PrintOut::ReadLine("input<any>", s);
		return true;
	}
}

int main(int argc, char* argv[])
{
	std::vector<json11::Json> jsonobj;
	if (argmental(argc,argv,jsonobj)) { return 1; }
	auto ahls = $ref<binauralLib>();
	for(auto& j:jsonobj)
	{
		if(j.is_object()==false)
		{
			continue;
		}
		auto& obj_0 =j.object_items();
		if (obj_0["file"].is_string()) 
		{
			auto value = Path(obj_0["file"].string_value()).full_path();
			auto init=ahls->Init(value);
			if (obj_0["pos"].is_array()) {
				ahls->of()->pos = Matrix3x3(obj_0["pos"]);
				if (init() == false)
				{
					break;
				}
			}
			if(obj_0["volume"].is_number())
			{
				ahls->SetVolume(obj_0["volume"].int_value());
			}
			if(obj_0["rate"].is_array()){
				ahls->Velcity(Vector3(obj_0["rate"]));
			}
			ahls->Play();
		}
	}
	return 0;
}

#if IS_WIN
OS::OS()
{
	__installer = "choco";
	std::string dir("C:\\ProgramData\\chocolatey\\bin");
	dir += __installer; dir += ".exe"; Path path(dir);
	if(path.isNotHaving())
	{
		system("powershell Start-Process PowerShell.exe -Verb runas Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')) ");
	}
}
#elif MACOSX
OS::OS()
{
	__installer = "brew";
}
#elif __linux__
OS::OS()
{
	__installer = "apt";
}
#endif
