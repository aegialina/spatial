LIB_JSON = ./libs/Kdjson/make.sfract
CC = clang
CXX = clang++

CFLAGS = -Wall -W -O2 -std=c++20

LD = clang++
LDFLAGS =

LIBS = -lavcodec -lavformat -lavutil -lswresample -lopenal -lalut
OUT_DIR=build

TARGET = ${OUT_DIR}/audio.soft
OBJS = audio.o
IMPORT = json11.o
BUILD= ${OUT_DIR}/audio.o ${OUT_DIR}/json11.o
INCLUDES =

#include ${LIB_JSON}

.SUFFIXES += .cpp 

%.o: %.cpp %.hpp
	$(CXX) $(CLFLAGS) -c $< -I $(INCLUDES) -o $@ || $(CXX) $(CLFLAGS) -c $< 

$(BUILD): $(OBJS) 
	mv $(IMPORT) $(OUT_DIR)/$(IMPORT)
	mv $(OBJS) $(OUT_DIR)/$(OBJS)

all:
	source build/build.sh

$(TARGET): $(BUILD)
	$(LD) $(LDFLAGS) $^ $(LIBS) -o $@

clean:
	source build/delete.sh